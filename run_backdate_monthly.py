#!/usr/bin/env python

import datetime
import sys
from dateutil.relativedelta import *
import dateutil.parser
import logging
import configure_logging
from single_export import SingleExport


def get_date_from_string(reference_date):
    return dateutil.parser.parse(reference_date).date()


def get_location_name():
    try:
        return sys.argv[1]
    except IndexError:
        message = "No country set - cannot carry on like this."
        logging.warning(message)
        sys.exit(message)


def get_start_date():
    try:
        return get_date_from_string(sys.argv[2])
    except IndexError:
        message = "No start date set - assuming today (i.e. set to first of this month)"
        logging.warning(message)
        return datetime.datetime.today().date()


def get_end_date():
    try:
        return get_date_from_string(sys.argv[3])
    except IndexError:
        message = "No end date set - assuming today (i.e. set to first of this month)"
        logging.warning(message)
        return datetime.datetime.today().date()


def generate_for_range(location_name, start_date, end_date):

    step = relativedelta(months=1)
    processing_date = start_date.replace(day=1)

    while end_date > processing_date:
        logging.info("...{0} {1}".format(location_name, processing_date))
        SingleExport(location_name, processing_date.strftime('%Y%m%d'))
        # Increment to first of next month
        processing_date = processing_date + step

    logging.info('Done!')

if __name__ == "__main__":
    configure_logging.configure_logging()
    logging.info(">>>> New run")
    location_name = get_location_name()
    start_date = get_start_date()
    end_date = get_end_date()

    logging.info('Starting generating first-of-monthly for {0}: {1} to {2}'.format(location_name, start_date, end_date))
    generate_for_range(location_name, start_date, end_date)
