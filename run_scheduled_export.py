#!/usr/bin/env python
# File to be run regularly by cron job to get updated layers for given countries

import datetime
import os

import configure_logging
from single_export import SingleExport

# Load list of countries (ISO3 codes)
countries_list_path = os.path.join(os.path.dirname(__file__), 'scheduled_countries.txt')
COUNTRIES = filter(None, open(countries_list_path).read().split('\n'))

# Assume want to run today
TODAY_DATE = datetime.date.today().strftime('%Y%m%d')

# SingleExport for every Country, using today's date
for country in COUNTRIES:
    SingleExport(country, TODAY_DATE)
