import os
import logging
import datetime
import errno


# Configures global logging via logging package
def configure_logging():
    timestamp = datetime.datetime.today().strftime("%Y%m%d-%H%M%S")
    filename = timestamp + '.log'
    log_filename = os.path.join(os.path.dirname(__file__), 'log', 
        filename)


    logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s',
                        filename=(log_filename),
                        level=logging.INFO)

    current_log_path = os.path.join(os.path.dirname(__file__), 'log', 'current.log')
    symlink_force(log_filename, current_log_path)


def symlink_force(target, link_name):
    try:
        os.symlink(target, link_name)
    except OSError, e:
        if e.errno == errno.EEXIST:
            os.remove(link_name)
            os.symlink(target, link_name)
        else:
            raise e