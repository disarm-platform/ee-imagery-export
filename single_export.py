#!/usr/bin/env python

import datetime
import sys
import logging
import configure_logging
from export_job import ExportJob


class SingleExport:
    """
    Starts an EarthEngine export process for given date and location.
    Takes the commandline args, unless they are explicitly set.
    """

    def __init__(self, location_name, reference_date):
        print('PROCESSING {0} for {1}'.format(location_name, reference_date))

        self.location_name = location_name
        self.country_bounding_box = None
    
        # Check input values roughly make sense
        if self.check_and_set_reference_date(reference_date):
            ExportJob(self.reference_date, self.location_name)
        else:
            logging.warning('Invalid date argument parsed:\ndate: {0}'.format(self.reference_date))


    def check_and_set_reference_date(self, reference_date):
        """
        :param reference_date: YYYYMMDD format, something like `20160504`
        :return: prints stuff

        Check the given date is in the range of 2000-01-01 to today.
        """
        earliest_date = datetime.date(2000, 01, 01)
        latest_date = datetime.date.today()

        # Parse date from given date_string
        ref_year = int(reference_date[0:4])
        ref_month = int(reference_date[4:6])
        ref_day = int(reference_date[6:8])

        given_date = datetime.date(ref_year, ref_month, ref_day)

        self.reference_date = (ref_year, ref_month, ref_day)
        return (given_date >= earliest_date) & (given_date <= latest_date)


if __name__ == "__main__":
    configure_logging.configure_logging()
    location_name = sys.argv[1]
    reference_date = sys.argv[2]
    SingleExport(location_name, reference_date)
