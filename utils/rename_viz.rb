# Renames all current images - they are based on 
# ```
#   task = ee.batch.Export.image(
#             summary_image.visualize(**item['viz_params']),
#             config=export_config
#         )
# # ```
# and we want them to be based on 
# ```
# task = ee.batch.Export.image(
#             summary_image.visualize(**item['viz_params']),
#             config=export_config
#         )
# ```

FILE_DUMP = "files_viz"

def files_list
  if (File.exists?(FILE_DUMP))
    files = nil
    File.open(FILE_DUMP,"rb") {|f| files = Marshal.load(f) }
    files
  else
    download_files_list
  end
end

def download_files_list
  files = `gsutil ls gs://ee-export/**/*`.split("\n")
  files.pop # Remove the last one which is just a directory
  File.open(FILE_DUMP, 'wb') do |file|
    Marshal.dump(files, file)
  end
  files
end

def rename_viz(file_path)
  parts = file_path.split '/'
  parts[0] = "gs://"
  parts.insert 4, 'viz'
  File.join(parts)
end

def process
  files_list.each do |file_path|
    from = file_path
    to = rename_viz(file_path)
    command = "gsutil mv #{from} #{to}"
    puts command
    `#{command}`
  end
end

process
