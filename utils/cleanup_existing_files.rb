# Don't re-run 
# Moves all files on gs://ee-export to subdirectories based on filename

def files_list
  if (File.exists?('files'))
    files = nil
    File.open("files","rb") {|f| files = Marshal.load(f) }
    files
  else
    download_files_list
  end
end

def download_files_list
  files = `gsutil ls gs://ee-export`.split("\n")
  files.pop # Remove the last one which is just a directory
  File.open('files', 'wb') do |file|
    Marshal.dump(files, file)
  end
end

def split_filename_and_dir(file_path)
  File.join(file_path.split(/([A-Z]{3})_/))
end

def process
  files_list.map do |file_path|
    from = file_path
    to = split_filename_and_dir(file_path)
    command = "gsutil cp #{from} #{to}"
    `#{command}`
  end
end