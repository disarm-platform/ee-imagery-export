import time
import os
import sys

import ee
import yaml
import logging

from country_bounding_boxes import (
    country_subunits_by_iso_code
)

from oauth2client.service_account import ServiceAccountCredentials


class ExportJob:
    def __init__(self, reference_date, location_name):

        self.config = self.configure_ee()

        # Grab arguments - could check them again?
        self.reference_date = ee.Date.fromYMD(reference_date[0],
                                              reference_date[1],
                                              reference_date[2])
        self.location_name = location_name

        self.set_bounding_box(self.location_name)

        self.loop_layers()

    def loop_layers(self):
        # Loop through the products, add them to the map and create an export task.
        for item in self.EXPORT_LIST:
            startdate = self.reference_date.advance(item['time_interval_start'], 'day')
            enddate = self.reference_date.advance(item['time_interval_end'], 'day')
            collection = item['collection'].filterDate(startdate, enddate)
            summary_image = collection.reduce(item['reducer'])

            collection_size = self.print_status(collection, enddate, item, startdate)

            if collection_size == 0:
                logging.warning('Collection is empty. Nothing to export.')
            else:
                # logging.info('Starting to export...')
                # Extract the CRS and CRS Transform for a sample image in the collection
                self.start_export(collection, enddate, item, summary_image)

    def print_status(self, collection, enddate, item, startdate):
        collection_size = collection.size().getInfo()

        plan = ('SINGLE - "{0}" for {1}, from {2} to {3} (collection_size {4})'.format(
            item['name'],
            self.location_name,
            startdate.format('YYYY-MM-dd').getInfo(),
            enddate.format('YYYY-MM-dd').getInfo(),
            collection_size
        ))
        logging.info(plan)

        return collection_size

    def start_export(self, collection, enddate, item, summary_image):
        # Grab sample_image to infer CRS
        sample_image = ee.Image(collection.first())
        sample_band_info = sample_image.getInfo()['bands'][0]

        # Configure export parameters
        export_config = dict(
            outputBucket=self.GCS_BUCKET_NAME,
            outputPrefix=self.create_prefix(item),
            region=self.export_region.bounds().coordinates().getInfo(),
            crs='EPSG: 4326',
            scale=item['scale']
            #crs_transform=sample_band_info['crs_transform']
        )

        # Start batch task
        task = ee.batch.Export.image(
            summary_image,
            config=export_config
        )

        # Start the export task, and periodically poll for its status.
        print('task.id = {id}\n'.format(id=task.id))
        task.start()
        while (task.status()['state'] not in ['FAILED', 'COMPLETED']):
            # print(task.status())
            # if task.status()['state'] is 'RUNNING':
            #     sys.stdout.write('.')
            time.sleep(10)
            print 'EXPORT STATUS:\n{0}\n'.format(task.status())

        # If the task failed, print the error information.
        if task.status()['state'] == 'FAILED':
            logging.error('Failed: {0}'.format(task.status()))

        if task.status()['state'] == 'COMPLETED':
            logging.info('Completed: {0}'.format(task.status()))

    def set_bounding_box(self, location_name):
        with open(os.path.join(os.path.dirname(__file__), 'countries.yml'), 'r') as file:
            doc = yaml.load(file)
        countries = doc['countries']

        filtered_countries = filter(lambda country: country['name'] == location_name, countries)

        country_bounding_box = [c.bbox for c in country_subunits_by_iso_code(location_name)][0]
        if len(country_bounding_box) > 0:
            logging.info('Found bounding box for {0}: {1}'.format(location_name, country_bounding_box))
            self.export_region = ee.Geometry.Rectangle(country_bounding_box, None, geodesic=True).buffer(2000).bounds()
            return True
        else:
            # No bounding box, assume that country ISO code is not found
            message = 'Cannot find bounding box for {0}'.format(location_name)
            logging.warning(message)
            print(message)
            sys.exit()
            return False

    def create_prefix(self, item):
        # :return Single string of uppercase components for filename.
        country_prefix = self.location_name.upper() + "/"

        file_prefix = "_".join([
            self.reference_date.format('YYYY-MM-dd').getInfo(),
            item['name'].upper(),
        ])

        return country_prefix + file_prefix

    def configure_ee(self):
        # TODO: Remove this call to ee.Initialize() from a 'config' file...
        service_account = 'earthengine-508@disarm-platform.iam.gserviceaccount.com'
        pem_file_path = os.path.join(os.path.dirname(__file__), 'privatekey.pem')
        credentials = ServiceAccountCredentials.from_p12_keyfile(
            service_account,
            pem_file_path,
            # scopes='https://www.googleapis.com/auth/earthengine'
            scopes=['https://www.googleapis.com/auth/earthengine',
                    'https://www.googleapis.com/auth/devstorage.read_write']
        )

        ee.Initialize(credentials)

        # Configure Google Cloud Storage export location
        self.GCS_BUCKET_NAME = 'ee-export'

        # Define the visualization palettes.
        self.EVI_PALETTE = ['FFFFFF', 'CE7E45', 'DF923D', 'F1B555', 'FCD163',
                       '99B718', '74A901', '66A000', '529400', '3E8601',
                       '207401', '056201', '004C00', '023B01', '012E01',
                       '011D01', '011301']
        self.NDWI_PALETTE = ['000000', 'FFFF66', '00FFFF', '000099', '00000F']
        self.LST_PALETTE = ['000000', 'FFFF00']

        # Configure the layer list
        self.EXPORT_LIST = [
            {
                'name': 'evi',
                #'collection': ee.ImageCollection('MODIS/MYD09GA_EVI'),
                'collection': ee.ImageCollection('MODIS/MYD09GA_006_EVI'),
                'time_interval_start': -30,
                'time_interval_end': 0,
                'reducer': ee.Reducer.median(),
                'scale': 500,
                'viz_params': {'min': -1, 'max': 1, 'palette': self.EVI_PALETTE}
            },
            {
                'name': 'lst',
                'collection': ee.ImageCollection('MODIS/006/MOD11A2').select("LST_Day_1km"),#
                'time_interval_start': -30,
                'time_interval_end': 0,
                'reducer': ee.Reducer.median(),
                'scale': 500,
                'viz_params': {'min': 14000, 'max': 16000, 'palette': self.LST_PALETTE}
            },
            {
                'name': 'ndwi',
                #'collection': ee.ImageCollection('MODIS/MYD09GA_NDWI'),
                'collection': ee.ImageCollection('MODIS/MYD09GA_006_NDWI'),
                'time_interval_start': -30,
                'time_interval_end': 0,
                'reducer': ee.Reducer.median(),
                'scale': 500,
                'viz_params': {'min': -0.2, 'max': 0.5, 'palette': self.NDWI_PALETTE}
            },
            {
                'name': 'precip',
                'collection': ee.ImageCollection('UCSB-CHG/CHIRPS/PENTAD'),
                'time_interval_start': -60,
                'time_interval_end': -30,
                'reducer': ee.Reducer.max(),
                'scale': 5000,
                'viz_params': {'min': -0.2, 'max': 200, 'palette': self.NDWI_PALETTE}
            }
        ]
