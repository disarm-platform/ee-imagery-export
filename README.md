# DiSARM EarthEngine export manager

## Reference dates

Note that the dates are 'reference dates'. They mean 'the period up to the given date' - and will typically cover the period 30 days prior (some layers are 30 days with a lag of 30 days).

## Bounding boxes

Wow, this is not (yet) as easy as you might expect. Go to http://boundingbox.klokantech.com/, search for a country, then make the box 'a bunch' bigger than the country extent. Then it should be fine... Add this info to `countries.yml` for _everywhere_ you're interested in doing - otherwise it will not work.

Only 2 ways to call:

### 1. Scheduled job

This will mostly be used by the `cron` task (automated scheduler), rather than being run directly. 

Run `./run_scheduled_export.py` to run all exports for the given list of countries, with the reference date set to current date (but beware of timezones).

Configure the list of countries in `countries_list.txt` (one ISO code per line). 


### 2. Generate 'first-of-month' for given range

Should be able to run `./run_backdate_monthly.py COUNTRY_ISO START_DATE END_DATE`. 

- `COUNTRY_ISO` as ISO2 or ISO3 code for given country
- `START_DATE` and `END_DATE` in YYYYMMDD format.

For example, `./run_backdate_monthly.py SWZ 20161001 20161031` to generate for the first of the month every month from start to end.


### So, what does the actual work?

`run_single.py` takes a `COUNTRY_ISO` and a `REFERENCE_DATE`, runs for all the layers [TODO: Split out running of each layer], and uploads the results.


## Layer and output location configuration
This is stored in `config.py`. `ee.Initialize()` also lives in there.


## Parameters

- `REFERENCE_DATE` is in YYYYMMDD format
- `COUNTRY_ISO` is either ISO2 or ISO3 for a country.


## Configuring the service account
Need a file called `privatekey.pem` to be in the top-level directory, and to match the service account authorised to use EarthEngine.

## Development

Install virtualenv & set it up: 

```bash
 pip install virtualenv
 virtualenv venv

 # For bash
 source venv/bin/activate

 # For fish
 source venv/bin/activate.fish
```

Then install the packages:

```bash
 pip install -r requirements.txt
```


## Deployment

`ssh-keygen` on remote, and add key to 'Deployment keys' on Bitbucket
`sudo apt-get install build-essential libssl-dev libffi-dev python-dev`
`pip install -r requirements.txt`
`sudo apt-get install git`
`git clone git@bitbucket.org:disarm-platform/ee-imagery-export.git`
`gcloud compute copy-files privatekey.pem ee-export:~/ee-imagery-export` 
